# AI-DTI


This project is to develop a framework that predict activatory and inhibitory drug-target intersctions (DTIs).

You can predict new activatory and inhibitory DTIs using the pre-trained model or reproduce our results. 

---

## Quick start

 **Note**: This source code was tested in Linux (Ubuntu 20.04) on python 3.8.3



* Install [rdkit](http://www.rdkit.org/)
 
In our case, we installed rdkit using the [conda-forge](https://conda-forge.org/#about) RDKit package after installing [anaconda](https://www.anaconda.com/).

```
conda install -c conda-forge rdkit
```


* Install mol2vec and other packages at the root of the repository


```
pip install pip --upgrade
pip install git+https://github.com/samoturk/mol2vec
pip install -r requirements.txt
```


* Unzip `data_pretrained_model.zip` into current folder

### To predict Novel DTIs using pretrained model

* Run `run_AI_DTI_pretrained.py`. Options are:

```
 --output_file : Name of output file
 --compound_file: A '.csv' format file containing the ID and SMILES of the compounds to be predicted. 
                  The file should be at least 2 columns named `compound_id` and `SMILES`
 --target_file: A '.csv' format file containing the ID and mode of action of the protein targets to be predicted. 
                The file should be at least 2 columns named `protein_id` and `mode_of_action`.
 --threshold: Threshold of the our model, default = 0.5#. 
```
 
* Example


```
python run_AI_DTI_pretrained.py --output_file=DTI_result.csv --compound_file=.\pretrained_model\compound_input_example.csv --target_file=.\pretrained_model\target_integrated_dataset.csv
```

** Note ** : `The target_integrated_dataset.csv` file contains a list of all activatory and inhibitory targets included in the integrated dataset.

** Note ** : The default threshold shows a false discovery rate of approximately 20% in the integrated data set (when the relative number of negative samples is the same as the positive samples in the test set). To get a more accurate prediction result, use the thresholds of 0.82 or 0.70, which correspond to the false detection rates of 0.01 or 0.05, respectively.

You can also predict targets of interest by defining a new target set.


### Reproducing our results

* Run `run_AI_DTI.py`. Options are:
```
--dataset: Type of dataset to evaluate performance. Three choices: ref, ext, and int.
           Each choice means evaluating performance on the reference dataset, extended dataset, 
           and integrated dataset.
 --MOA: Type of MoA to predict. Two choices: act and inh. Each choice means evaluating 
        performance for activatory and inhibitory DTIs
 --neg_ratio: Relative number of negative samples to positive samples in the test set. 
              Note that the weight of positive and negative samples is the same in the training set 
              regardless of this parameter. Two choices: int (i.e., integer) and all, the former one sets 
              the positive:negative = 1:int, the latter one considers all unknown DTIs as negative samples.  
```

* Example

```
python run_AI_DTI.py --dataset=ref --MOA=act --neg_ratio=1


```

## Data specification

* com_mol2vec_25282.csv: Compound feature matrix that embedded vectors of small molecules belonging to TTD and drugbank using mol2vec
* L1000_corr_weighted_mean(raw+inferred_10443).csv: Target feature matrix that calculated or inferred representative feature vector using weighted average procedure
* TTD_DTIs(DTI_id).csv: A list of drug-target interactions obtained from the therapeutic target database 2.0 and then subjected to ID mapping of compounds and targets

## Contacts
Please send any questions you might have about this repository to <lwy21@gachon.ac.kr> or <gachonphysiology@gmail.com>
