# -*- coding: utf-8 -*-
"""
Created on Sat Mar 13 20:29:08 2021

@author: yung
"""


import argparse
import pandas as pd
from sklearn.model_selection import StratifiedKFold
from deepforest import CascadeForestClassifier

from AI_DTI import preprocessing
from AI_DTI import performance_evaluation


# from mol2vec.features import mol2alt_sentence, MolSentence, DfVec, sentences2vec
# from gensim.models import word2vec
# from rdkit import Chem

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--dataset', required=False, default = 'int', help ='Type of dataset to perform prediction i.e., ref or ext or int')
    parser.add_argument('--MOA', required=True, help ='Which mode-of-action to predict i.e., act or inh')
    parser.add_argument('--neg_ratio', required=False, default = 1 , help ='ratio of negative sample i.e., int or all')
           
    options = parser.parse_args()
 
    dataset_type = options.dataset
    MOA = options.MOA
    if options.neg_ratio == 'all':
        neg_ratio = options.neg_ratio 
    else:
        neg_ratio = int(options.neg_ratio)

    print("""Predicting activatory and inhibitory DTIs based on an optimized CDF model 
        (dataset_type = %s, mode_of_action = %s, negative_sample_ratio = %s)"""
        % (dataset_type, MOA, neg_ratio))
        
    DTI_TTD_file = './data/TTD_DTIs(DTI_id).csv'
    com_feature_file = './data/com_mol2vec_25282.csv'
    L1000_file = './data/L1000_corr_weighted_mean(raw+inferred_10443).csv'
    
    vec_com = pd.read_csv(com_feature_file, index_col =0, low_memory=False).drop(columns = ['TTD_id', 'drugbank_id'])
    vec_com = vec_com.drop(columns = vec_com.columns[0:4])
    vec_com = vec_com[~vec_com.index.duplicated(keep='first')].astype('float16')  # 중복된 index 삭제
    
    DTI_df = pd.read_csv(DTI_TTD_file, index_col=0)
    vec_tar_raw = pd.read_csv(L1000_file, index_col=0)

    X, y = preprocessing.DTI_dataset_generation(DTI_df, vec_com, vec_tar_raw, dataset_type, MOA, neg_ratio)
    
    # model setting
    skf = StratifiedKFold(n_splits=5, shuffle=True, random_state = 2)

    print('Performance evaluation on 5-cross validation')
    
    AUROC_AUPR_df = pd.DataFrame(columns = ['AUROC', 'AUPR'])
    
    i=0
    for train_idx, test_idx in skf.split(X, y):

        i += 1
        X_train, X_test, y_train, y_test = X[train_idx,:], X[test_idx,:], y[train_idx], y[test_idx]
        CDF_opt = CascadeForestClassifier(n_trees = 500, n_estimators=8, n_jobs=-4, verbose= 0)
        CDF_opt.fit(X_train, y_train)
        AUROC, AUPR = performance_evaluation.AUPRC_AUPR(CDF_opt, X_test, y_test)

        print("AUROC and AUPR of %s-fold is %s, and %s" %(i, AUROC, AUPR))
        
        AUROC_AUPR_df.loc[i] = [AUROC, AUPR]
    
    sd = AUROC_AUPR_df.std(axis=0)
    mean = AUROC_AUPR_df.mean(axis=0)
    
    print('AUROC and AUPR is %s±%s and %s±%s' % (mean.iloc[0], sd.iloc[0], mean.iloc[1], sd.iloc[1]) )
    
    