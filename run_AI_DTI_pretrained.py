# -*- coding: utf-8 -*-
"""
Created on Sat Mar 13 14:31:18 2021

@author: yung
"""

import argparse, pickle; 
import pandas as pd; import numpy as np


from mol2vec.features import mol2alt_sentence, MolSentence, DfVec, sentences2vec
from gensim.models import word2vec

from rdkit import Chem

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--output_file', required=True, help ='Output file name')
    parser.add_argument('--compound_file', required=True, help ='Input compound file')
    parser.add_argument('--target_file', required=True, help ='Output directory')
    parser.add_argument('--threshold', required= False, default = 0.5, help ='Threshold of DTI interaction score')
    
    print("""Predicting activatory and inhibitory DTIs using pretrained AI-DTI method 
        (based optimized CDF model trained on integrated dataset)""")
    
    options = parser.parse_args()
 
    output_file = options.output_file
    input_compound_file = options.compound_file
    input_target_file = options.target_file
    threshold = options.threshold

    
    mol2vec_model_path = './data/mol2vec_300dim.pkl'
    model = word2vec.Word2Vec.load(mol2vec_model_path)
    
    with open('./pretrained_model/AI_DTI_act.pkl', 'rb') as f:
        AI_DTI_act = pickle.load(f)

    with open('./pretrained_model/AI_DTI_inh.pkl', 'rb') as f:
        AI_DTI_inh = pickle.load(f)


    L1000_file_path = './data/L1000_corr_weighted_mean(raw+inferred_10443).csv'
    vec_tar_raw = pd.read_csv(L1000_file_path, index_col=0)
    vec_tar_act_index = vec_tar_raw['pert_type'].isin(['trt_oe', 'trt_oe(inf)'])
    vec_tar_act = vec_tar_raw.loc[vec_tar_act_index].set_index('pert_iname').drop(columns = 'pert_type')
    vec_tar_inh_index = vec_tar_raw['pert_type'].isin(['trt_sh.cgs', 'trt_sh.cgs(inf)'])
    vec_tar_inh = vec_tar_raw.loc[vec_tar_inh_index].set_index('pert_iname').drop(columns = 'pert_type')

    
    compound_df = pd.read_csv(input_compound_file)
    target_df = pd.read_csv(input_target_file)
    
    
    # Generation of compound feature vector 
    df_vec = pd.DataFrame()
    SMILES_list = compound_df['SMILES']
    
    SMILES_list_ECFP_calculable = []
    SMILES_list_ECFP_non_calculable = []

    for i in range(len(SMILES_list)):
        try:
            MolSentence(mol2alt_sentence(Chem.MolFromSmiles(SMILES_list[i]), 1))
            SMILES_list_ECFP_calculable.append(SMILES_list[i])
        except:
            SMILES_list_ECFP_non_calculable.append(compound_df['compound_id'].iloc[i])

    if len(SMILES_list_ECFP_non_calculable) > 0 :
        print('The compound(s) that can not compute the ECFP (i.e., %s) was excluded from DTIs prediction' % (SMILES_list_ECFP_non_calculable))
    
    compound_df = compound_df.loc[compound_df['SMILES'].isin(SMILES_list_ECFP_calculable)]
    
    df_vec['compound_id'] = compound_df['compound_id']  
    df_vec['SMILES'] = compound_df['SMILES']
    df_vec['Mol'] = [Chem.MolFromSmiles(x) for x in compound_df['SMILES']]
       
    df_vec['sentence'] = df_vec.apply(lambda x: MolSentence(mol2alt_sentence(x['Mol'], 1)), axis=1)
    df_vec['mol2vec'] = [DfVec(x) for x in sentences2vec(df_vec['sentence'], model, unseen='UNK')]
    
    compound_vector = np.array([x.vec for x in df_vec['mol2vec']])
    compound_id = list(compound_df['compound_id'])
    
    # Generation of target feature vector
    
    tar_list_act_raw = target_df['protein_id'].loc[target_df['mode_of_action'] == 'activation']
    tar_list_act = set(tar_list_act_raw).intersection(vec_tar_act.index)

    if len(tar_list_act) != len(tar_list_act_raw):
        tar_list_act_out = tar_list_act - set(tar_list_act_raw)
        print('The protein target(s) that can not available transcriptome data was (were) excluded from activatory DTIs prediction')
        
    vec_tar_act_interest = vec_tar_act.loc[tar_list_act]
    
       
    tar_list_inh_raw = target_df['protein_id'].loc[target_df['mode_of_action'] == 'inhibition']
    tar_list_inh = set(tar_list_inh_raw).intersection(vec_tar_inh.index)

    if len(tar_list_inh) != len(tar_list_inh_raw):
        tar_list_inh_out = tar_list_inh - set(tar_list_inh_raw)
        print('The protein target(s) that can not available transcriptome data was (were) excluded from inhibitory DTIs prediction')
        
    vec_tar_inh_interest = vec_tar_inh.loc[tar_list_inh]
    
        
    # Predicting activatory and inhibitory DTIs based on constructed features
    
    DTI_prediction_result = pd.DataFrame()
    
    
    for i in range(len(compound_vector)):
        
        print('\nPredicting activatory and inhibitory DTIs of %s' %(compound_id[i]))
        
        DTI_score_act = pd.DataFrame()
        
        com_feature = np.ones((len(vec_tar_act_interest),1))* compound_vector[i,:]
        com_tar_feature = np.concatenate([com_feature, vec_tar_act_interest],axis=1)
        score = AI_DTI_act.predict_proba(com_tar_feature)[:,1]

        DTI_score_act['score'] = score
        DTI_score_act['protein_id'] = tar_list_act
        DTI_score_act['compound_id'] = compound_id[i]
        DTI_score_act['MOA'] = 'activation'

        DTI_score_act = DTI_score_act.loc[DTI_score_act['score']>=threshold]
    
        DTI_score_inh = pd.DataFrame()
        
        com_feature = np.ones((len(vec_tar_inh_interest),1)) * compound_vector[i,:]
        com_tar_feature = np.concatenate([com_feature, vec_tar_inh_interest], axis=1)
        score = AI_DTI_inh.predict_proba(com_tar_feature)[:,1]
        
        DTI_score_inh['score'] = score
        DTI_score_inh['protein_id'] = tar_list_inh
        DTI_score_inh['compound_id'] = compound_id[i]
        DTI_score_inh['MOA'] = 'inhibition'

        DTI_score_inh = DTI_score_inh.loc[DTI_score_inh['score'] >= threshold]
        
        DTI_prediction_result = pd.concat([DTI_prediction_result, DTI_score_act, DTI_score_inh], ignore_index= True)
   
    DTI_prediction_result.to_csv(output_file)
    
    DTI_act = DTI_prediction_result.loc[DTI_prediction_result['MOA'] == 'activation']
    DTI_inh = DTI_prediction_result.loc[DTI_prediction_result['MOA'] == 'inhibition']
        
    print ("""AI-DTI model predicted %s activatory DTIs between %s compounds and %s targets
        and predicted %s inhibitory DTIs between %s compounds and %s targets """ 
                %(len(DTI_act), len(pd.unique(DTI_act['compound_id'])), len(pd.unique(DTI_act['protein_id'])),
                  len(DTI_inh), len(pd.unique(DTI_inh['compound_id'])), len(pd.unique(DTI_inh['protein_id']))
                  )
            )
    
    