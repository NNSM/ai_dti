# -*- coding: utf-8 -*-
"""
Created on Sat Mar 13 20:28:43 2021

@author: yung
"""

import numpy as np; import pandas as pd;
from sklearn import metrics 
    
def pert_type_dict_generation():
    """
    Generate pert_type_dict

    Returns
    -------
    pert_type_dict : dict
        Dictionary that contain pert_type list in L1000 array

    """
    
    ### assign MoA & pert type dict
    MOA_dict = {}
    MOA_dict['act'] = ['Activator', 'Agonist']      # 활성 표적의 MOA
    MOA_dict['inh'] = ['Inhibitor', 'Antagonist']   # 억제 표적의 MOA
    
    pert_type_dict = {}
    pert_type_dict['act'] = {}
    pert_type_dict['act']['ref'] = ['trt_oe']
    pert_type_dict['act']['ext'] = ['trt_oe(inf)']
    pert_type_dict['act']['int'] = ['trt_oe', 'trt_oe(inf)']
    
    pert_type_dict['inh'] = {}
    pert_type_dict['inh']['ref'] = ['trt_sh.cgs']
    pert_type_dict['inh']['ext'] = ['trt_sh.cgs(inf)']
    pert_type_dict['inh']['int'] = ['trt_sh.cgs', 'trt_sh.cgs(inf)']
    
    return MOA_dict, pert_type_dict


def pos_sample_selection(DTI_df, MOA_of_interest, dataset_type, vec_tar):
    """
    Select positive sample of interest 
    
    
    Parameters
    ----------
    DTI_df : pandas dataframe
        A dataframe that contain drug('CID')-target('Gene_symbol') interaction
    MOA_of_interest : str ('act' or 'inh')
        MoA of interest to select. 
        (activatory and inhibitory DTIs dataset for 'act' or 'inh', respectively)
    dataset_type : str ('ref', 'ext' or 'int')
        Dataset of interest to select 
        (reference, extended, and integrated dataset for 'ref', 'ext', or 'int', respectively)

    vec_tar : pandas dataframe
        target vector representation from L1000 dataset

    Returns
    -------
    df_DTI : positive DTIs
        DESCRIPTION.

    """
    
    MOA_dict, pert_type_dict = pert_type_dict_generation()
    
    # Assign index for predictable targets
    pert_type_of_interest = pert_type_dict[MOA_of_interest][dataset_type]
    index_of_interest = vec_tar['pert_type'].isin(pert_type_of_interest)
      # 해당하는 pert type과 관련된 index 선택
    predictable_targets = vec_tar['pert_iname'].loc[index_of_interest]
      # 해당 pert type의 target 선택 

    ### assign MoA & pert type dict


    df_DTI = DTI_df.loc[DTI_df['MOA'].isin(MOA_dict[MOA_of_interest])]  
      # select MoA of interest
    df_DTI = df_DTI.loc[df_DTI['Gene_symbol'].isin(predictable_targets)]
      # select predictable targets
    # df_DTI = df_DTI.loc[df_DTI['SMILES_available'] == 1].drop_duplicates()                
    #   # drug compound that available 2d structure information 
    
    
    return df_DTI
    
    
def neg_sample_generation(pos_DTI_df, non_pos_ratio):
    """
    Generage negative sample based on positive DTI sample 


    Parameters
    ----------
    pos_DTI_df : dataframe TYPE
        Dataframe that contain drug ('CID') and target ('Gene_symbol') for compound and target, respectively
    non_pos_ratio : int or 'all'
        Set negative sample ratio
        If select 'all', negative sample will be remaining all non-pos sample

    Returns
    -------
    DTI_set : pandas dataframe
        DTIs dataset with negative sample.
        negative samples were randomly selected from non-positive samples
        
    """
    
    # Prepare DTI, com, and tar list
    
    DTI_pos = pos_DTI_df[['CID', 'Gene_symbol']].drop_duplicates()

    DTI_pos['CID'] = DTI_pos['CID'].map(int).map(str)
    
    list_tar = DTI_pos['Gene_symbol'].drop_duplicates().reset_index(drop=True)
    list_com = DTI_pos['CID'].drop_duplicates().reset_index(drop=True)
    
    DTI_pos['label'] = 1
    
    DTI_pos_index = []
    
    for i in range(len(DTI_pos)):
        index_com = np.where(list_com == DTI_pos['CID'].iloc[i])[0][0]
        index_tar = np.where(list_tar == DTI_pos['Gene_symbol'].iloc[i])[0][0]
        index_flatten = index_com * len(list_tar) + index_tar
        DTI_pos_index.append(index_flatten)
    
    # flatten index 범주내에서 sampling 후 중복 sample 제거 
    
    if str(non_pos_ratio) == 'all':
        DTI_random_index = list(set([i for i in range(len(list_tar)*len(list_com))]) - set(DTI_pos_index))
        # non sample : positive 아닌 모든것
        
    else:
            
        DTI_random_cand = np.random.randint(0, len(list_tar)*len(list_com), len(DTI_pos_index)*non_pos_ratio*2)
         # 2배수 뽑음 (DTI_pos와 겹칠수도 있음 + 중복 sampling)
        DTI_random_index = list(set(DTI_random_cand) - set(DTI_pos_index))[0:non_pos_ratio*len(DTI_pos_index)]
    
     # pos : neg = pos : all-non positive 상황일 때
    
    DTI_non_pos_dict = {}
    
    for i in range(len(DTI_random_index)):
        index_com = DTI_random_index[i]//len(list_tar)    
        index_tar = DTI_random_index[i]%len(list_tar)
        
        DTI_non_pos_dict[i] = [list_com[index_com], list_tar[index_tar]]
        
        # if (i%100000 ==0):
        #     print(' Generate %sth non_pos DTI index (%s/%s)' %(i,i, len(DTI_random_index)))
            
    DTI_non_pos = pd.DataFrame(DTI_non_pos_dict).T
    DTI_non_pos.rename(columns= {0 : 'CID', 1:'Gene_symbol'}, inplace=True)
    
    
    ## label 붙인 후 concatenation
    DTI_non_pos['label'] = 0
    
    DTI_set = pd.concat([DTI_pos,DTI_non_pos], axis=0, ignore_index=True)    
    
    return DTI_set
    

def DTIs_vectorization (pos_neg_DTI_df, com_df, tar_df, MOA_of_interest, dataset_type):
    """
    Generate input matrix for training machine learning model

    Parameters
    ----------
    DTI_df : pandas dataframe
        Dataframe which contain drug (colname 'CID')-target ('Gene_symbol')-label ('label')
    com_df : pandas dataframe
        Dataframe which contain compound vector representation using mol2vec
    tar_df : pandas dataframe
        Dataframe which contain target vector representation from L1000

    Returns
    -------
    DTI_vectorize : pandas dataframe
        Concatenated vector using compound-target vector
    DTI_label : pandas dataframe
        DTIs label (1 - positive, 0 - negative)

    """
        
    
    ## 한번에 뽑아서 정렬
    
    com_index = [int(i) for i in pos_neg_DTI_df['CID']]
    com_df_input = com_df.loc[com_index].reset_index(drop=True)
        
    # index_of_interest = vec_tar['pert_type'].isin(pert_type_of_interest)
    #   # 해당하는 pert type과 관련된 index 선택
    
    # vec_tar = vec_tar.loc[index_of_interest]
    # vec_tar = vec_tar.set_index('pert_iname').drop(columns = 'pert_type').astype('float16') 
    
    tar_index = [i for i in pos_neg_DTI_df['Gene_symbol']]
    # tar_df = vec_tar.loc[tar_index].reset_index(drop=True)
    tar_df = tar_df.loc[tar_index].reset_index(drop=True)
    
    DTI_vectorize = np.array(pd.concat([com_df_input, tar_df], axis=1, ignore_index=True))
    
    DTI_label = np.array(pos_neg_DTI_df['label'])
    
    return DTI_vectorize, DTI_label




def DTI_dataset_generation(DTI_df, com_df, vec_tar, dataset_type, MOA_of_interest, non_pos_ratio):
    """
    Generate DTI dataset containing negative samples

    Parameters
    ----------
    DTI_df : pandas dataframe
        DESCRIPTION.
    target_df : pandas dataframe
        DESCRIPTION.
    dataset_type : 
        DESCRIPTION.
    MOA_of_interest : TYPE
        DESCRIPTION.
    non_pos_ratio : TYPE
        DESCRIPTION.

    Returns
    -------
    DTIs_pos_neg : TYPE
        DESCRIPTION.

    """    
    
    vec_tar_act_index = vec_tar['pert_type'].isin(['trt_oe', 'trt_oe(inf)'])
    vec_tar_act = vec_tar.loc[vec_tar_act_index].set_index('pert_iname').drop(columns = 'pert_type')
    vec_tar_inh_index = vec_tar['pert_type'].isin(['trt_sh.cgs', 'trt_sh.cgs(inf)'])
    vec_tar_inh = vec_tar.loc[vec_tar_inh_index].set_index('pert_iname').drop(columns = 'pert_type')
    
    if MOA_of_interest == 'act':
        tar_df = vec_tar_act
    else:
        tar_df = vec_tar_inh
    
    pos_DTI_df = pos_sample_selection(DTI_df, MOA_of_interest, dataset_type, vec_tar)
    pos_neg_DTI_df = neg_sample_generation(pos_DTI_df, non_pos_ratio)
    
    DTI_vectorized, DTI_label = DTIs_vectorization (pos_neg_DTI_df, com_df, tar_df, MOA_of_interest, dataset_type)

    # pos_DTI_df = pos_sample_selection(DTI_df, MOA_of_interest, dataset_type, vec_tar)
    
    if non_pos_ratio == 'all':
        neg_sample_num = len(pos_DTI_df['CID'].unique()) * len(pos_DTI_df['Gene_symbol'].unique()) - len(pos_DTI_df.index)
    else:
        neg_sample_num = len(pos_DTI_df.index)*non_pos_ratio
    
    print ("""  Select %s DTIs in %s dataset 
        Dataset contain %s positive DTIs and %s negative DTIs between %s drug and %s targets. """         
        % (MOA_of_interest, dataset_type, len(pos_DTI_df.index), neg_sample_num, len(pos_DTI_df['CID'].unique()), len(pos_DTI_df['Gene_symbol'].unique())))
    
    
    
    # DTIs_pos_neg = neg_sample_generation(pos_DTI_df, non_pos_ratio)
    # DTIs_vectored, DTI_label = DTIs_vectorization(DTIs_pos_neg, com_df, vec_tar, MOA_of_interest, dataset_type)
    
    
    return DTI_vectorized, DTI_label



def DTI_generation_from_pos_sample(pos_DTI_df, com_df, vec_tar, dataset_type, MOA_of_interest, non_pos_ratio):
    """
    Generate DTI dataset containing negative samples

    Parameters
    ----------
    DTI_df : pandas dataframe
        DESCRIPTION.
    target_df : pandas dataframe
        DESCRIPTION.
    dataset_type : 
        DESCRIPTION.
    MOA_of_interest : TYPE
        DESCRIPTION.
    non_pos_ratio : TYPE
        DESCRIPTION.

    Returns
    -------
    DTIs_pos_neg : TYPE
        DESCRIPTION.

    """    
    
    vec_tar_act_index = vec_tar['pert_type'].isin(['trt_oe', 'trt_oe(inf)'])
    vec_tar_act = vec_tar.loc[vec_tar_act_index].set_index('pert_iname').drop(columns = 'pert_type')
    vec_tar_inh_index = vec_tar['pert_type'].isin(['trt_sh.cgs', 'trt_sh.cgs(inf)'])
    vec_tar_inh = vec_tar.loc[vec_tar_inh_index].set_index('pert_iname').drop(columns = 'pert_type')
    
    if MOA_of_interest == 'act':
        tar_df = vec_tar_act
    else:
        tar_df = vec_tar_inh
    
    # pos_DTI_df = pos_sample_selection(DTI_df, MOA_of_interest, dataset_type, vec_tar)
    print("Generate negative sample by randomly selecting non-positive sample")
    pos_neg_DTI_df = neg_sample_generation(pos_DTI_df, non_pos_ratio)

    print("Generate input matrix for model training")
    DTI_vectorized, DTI_label = DTIs_vectorization (pos_neg_DTI_df, com_df, tar_df, MOA_of_interest, dataset_type)


    print ("""  Select %s DTIs in %s dataset 
        Dataset contain %s positive DTIs and %s negative DTIs between %s drug and %s targets. """         
        % (MOA_of_interest, dataset_type, len(pos_DTI_df.index), len(pos_DTI_df.index)*non_pos_ratio, len(pos_DTI_df['CID'].unique()), len(pos_DTI_df['Gene_symbol'].unique())))
    
    return DTI_vectorized, DTI_label


# Reduce negative samples with the matching number as positive samples

def matching_pos_neg_index (train_index, y_train):
    where_train_pos_index = np.where(y_train == 1)[0]
    train_pos_index = train_index[where_train_pos_index]
    
    where_train_neg_index = np.where(y_train == 0)[0]
    train_neg_index = train_index[where_train_neg_index]
    
    if len(train_pos_index) >= len(train_neg_index):
        train_index_reduced = train_index
    else:
        train_neg_index_mat = np.random.choice(train_neg_index, len(train_pos_index), replace=False)
        train_index_reduced = np.sort(np.concatenate([train_pos_index, train_neg_index_mat]))
        
    return train_index_reduced 


def AUPRC_AUPR (clf, X_test, y_test):

    y_prob = clf.predict_proba(X_test)[:,1]
    fpr, tpr, threshold = metrics.roc_curve(y_test, y_prob, pos_label=1)
    auc= metrics.auc(fpr,tpr)
    auprc = metrics.average_precision_score(y_test, y_prob, pos_label=1)
    
    return auc, auprc


def AUPRC_AUPR_CFR (clf, X_test, y_test):

    y_prob = clf.predict(X_test)
    fpr, tpr, threshold = metrics.roc_curve(y_test, y_prob, pos_label=1)
    auroc= metrics.auc(fpr,tpr)
    aupr = metrics.average_precision_score(y_test, y_prob, pos_label=1)
    
    return auroc, aupr